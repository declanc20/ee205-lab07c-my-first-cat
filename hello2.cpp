/////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07 - My First Cat - EE 205 - Spr 2022
/// @file countdown.cpp
/// @version 1.0
///
///file: hello2.cpp
/// desrcip: prints out hello world without using namespace std;
//
//
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/26/22
///////////////////////////////////////////////////////////////////////////////

#include<iostream>

int main() {


   std::cout<<"Hello World" << std::endl;


return 0;
}
