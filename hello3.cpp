/////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07 - My First Cat - EE 205 - Spr 2022
/// @file countdown.cpp
/// @version 1.0
///
///file: hello3.cpp
/// desrcip: the cat says meow
//
//
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/26/22
///////////////////////////////////////////////////////////////////////////////

#include<iostream>

using namespace std;

class Cat {
public: 
      void sayHello() { 
      cout<<"Meow"<<endl;  //print meow to screen
      }
};


int main() {

   Cat myCat; // instantiate cat object
   myCat.sayHello();

return 0;
}
